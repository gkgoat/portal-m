// AFTER _$runtime
struct _theRuntime: _$runtime{
    WASI_DEFINE();
};
using theRuntime = std::shared_ptr<_$runtime>;