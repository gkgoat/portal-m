#include <cstdint>
#include <memory>
#include <map>
#include <functional>
struct BaseApi{
    virtual ~BaseApi(){}
    virtual uint64_t poke(uint64_t a, std::function<uint8_t(uint64_t)> r,std::function<void(uint64_t,uint8_t)>) = 0;
};
template<typename Ret,typename ...Args> Ret call(void *f,Args ...a){
    std::function<Ret(Args...)> &g = *(std::function<Ret(Args...)>*)f;
    return g(a...);
}
template<typename It> struct ApiPatch: It, BaseApi{
    theRuntime rt;
    std::shared_ptr<std::map<uint64_t,std::shared_ptr<BaseApi>>> m;
    uint64_t poke(uint64_t it, uint64_t a, uint8_t(*rv)(void*,uint64_t), void *rr, void(*wv)(void*,uint64_t,uint8_t), void *ww) override{
        auto r = [&](uint64_t x) -> uint8_t{return rv(rr,x);};
        auto w = [&](uint64_t a,uint8_t b){wv(ww,a,b);};
        if(it == 0)return rt.poke(a, call<uint8_t,uint64_t>, r, call<void, uint64_t,uint8_t>, w);
        if((*m).contains(it))return (*m)[it]->poke(a,r,w);
        return rt.regen(CORE_CONTINUE());
    }
    uint64_t poke(uint64_t a, std::function<uint8_t (uint64_t)> r, std::function<void (uint64_t, uint8_t)> w) override{
        return peek(a, call<uint8_t,uint64_t>, r, call<void,uint64_t,uint8_t>, w);
    }
};