#include <string>
#include <vector>
template<typename Tools> std::string exec_wasm2c(Tools &t, std::vector<uint8_t> v,std::string name){
    auto c = t.Wasm2c(v,name);
    return c.Source + GenExports(c.Imports,c.Exports,name);
}
std::string GenExports(std::vector<std::string> x,std::vector<std::string> xx, std::string name){
    std::string r = "struct _$";
    r += name;
    r += "{virtual ~_$";
    r += name;
    r += "{Z_";
    r += name;
    r += "_free(my);};";
    r += "_$";
    r += name;
    r += "(){Z_";
    r += name;
    r += "_instantiate(this, my);}";
    r += name;
    for(auto y: x){
        r += "PORTAL_DEFUN(" + y + ");";
    }
    r += "std::shared_ptr<Z_";
    r += name;
    r += "_instance_t> my = std::make_shared<>();";
    for(auto y: xx){
        r += "template<typename... Args> auto ";
        r += y;
        r += "(Args ...args){return Z_";
        r += name;
        r += "_";
        r += y;
        r += "(my, args...);};";
    }
    r += "};";
    for(auto y: x){
        r += "auto Z_";
        r += name;
        r += "Z_##PORTAL_SIG(";
        r += name;
        r += ",";
        r += y;
        r += "){";
        r += "return PORTAL_POKE($, ";
        r += y;
        r += ")";
    }
    return r;
}